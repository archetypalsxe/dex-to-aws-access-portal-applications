# Authenticating Kubernetes Apps with AWS Identity Center (SSO) and Dex (OIDC)

This guide will walk you through deploying and setting up a standalone [Dex instance](https://dexidp.io/), a federated OpenID Connect (OIDC) provider, to authenticate Kubernetes applications using AWS Identity Center (SSO). We’ll use Argo Workflows as a practical example for a SAML-based setup. The concepts discussed here can be applied to other applications and authentication methods like OAuth 2.0. If there’s enough interest, I’d be happy to provide a follow-up article focusing on OAuth 2.0 integration.

## Background

In a recent project, I integrated Argo Workflows into our Kubernetes clusters, which already had Argo CD installed. Initially, I assumed both Argo Workflows and Argo CD would share a unified user interface (UI) and authentication system. However, I soon discovered they operate separately—each with its own UI, authentication methods, and [distinct role-based access control systems](https://github.com/argoproj/argo-workflows/discussions/7569) (Argo CD uses Casbin, while Argo Workflows relies on native Kubernetes RBAC).

Although Dex is included in the Helm chart for installing Argo CD, and there’s [documentation](https://argo-workflows.readthedocs.io/en/stable/argo-server-sso-argocd/) on configuring the shared Dex server, we chose to deploy Dex as a standalone instance. This approach provides greater flexibility, enabling us to scale authentication across multiple Kubernetes applications and upgrade Dex independently of other applications.

For OAuth 2.0 authentication with IAM Identity Center (AWS SSO), your identity source must be either an IAM Identity Center directory or an external identity provider. Unfortunately, AWS Managed Microsoft Active Directory is not a trusted token issuer, which I understand... I don't trust Microsoft very much either. **It's advisable to use OAuth if possible, as the SAML connector in Dex is unmaintained and is being considered for deprecation.**

While the available documentation for Argo and Dex was helpful, it did not fully address my needs, requiring significant trial and error to find a workable solution. You may encounter similar challenges if you're working on integrating Kubernetes applications and hopefully this article proves useful. If there is enough interest, I plan to publish a follow-up or update this post with a more comprehensive guide on setting up OAuth and detailed templates from scratch.

## Setting Up The IAM Identity Center Application

### Pre Reqs

You need to have an identity source setup in the IAM Identity Center. If you go to the `IAM Identity Center` in the AWS console, you can find this under the `Settings` page. You should set one up, if you have not already. If you use an `AWS Managed Microsoft Active Directory`, you can only add applications using SAML 2.0 **(which should be avoided if at all possible)**. If you're using either Identity Center directory or an External Identity Provider, you'll be able to use the preferred OAuth.

### Adding Application

We need to setup the application in AWS IAM Identity Center, so that we can obtain the client certificate. This can be done by going to the `IAM Identity Center` in the AWS console, `Applications` under `Application assignments`, and selecting `Add application`. The display name and description should be human readable, something like `SAML Application`. You'll want to make sure and download the `IAM Identity Center Certificate` which we will use later on in the client certificates Kubernetes secret.

The application start URL will be the hostname for the application (client) with `/oauth2/redirect` as a postfix. In my example, it would be in [this file](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/blob/main/kustomize/saml-application/ing.ingress.yaml) and would be `SAML-DOMAIN.com/oauth2/redirect`

For both the `Application ACS URL` and `Application SAML audience`, you will want to use the Dex callback URL with `/dex/callback` as a postfix. In my [example file](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/blob/main/kustomize/dex/ing.dex.yaml), it would be `dex-domain.com/dex/callback`

You will want to copy down the "IAM Identity Center sign-in URL" and the "IAM Identity Center SAML Issuer URL", although there is a good chance they will be the same. You are able to retrieve these later on if needed.

### Attribute Mappings

After creating the application, you should add attribute mappings. I have forgotten to do this more than once, and without it, the authentication will not work. This can be done by clicking on the `Actions` dropdown and selecting `Edit attribute mappings`. The Subject should map to the string value `${user:email}` and it should be persistent. You then want to add an attribute mapping. The user attribute should be `email`, which should also map to the string value `${user:email}` and the format will be `basic`

## Client Secrets

In my repository, I have it setup (at least currently) assuming that only one application is going to be added. It's setup in a way that it would be trivial to add additional applications. There will be a client key secret in Kubernetes specific to each client. There is also a single client cert secret in Kubernetes that will have a key/value pair for each client.

### Client Keys

Each client will have its own client key. In my [example](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/blob/main/kustomize/dex/externalsecret.saml-application-key.yaml), it's called `saml-application`. The key secret should have a field for client-id and client-key. The client-id should be something human readable (like `saml-application`) while the client-key should be a random string, essentially a password. Dex uses these to allow the clients to authenticate against it when combined with the client certificate.

### Client Cert

There should only be a single client certificate secret for Dex, but there should be an entry in the secret for each client. In our [example](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/blob/main/kustomize/dex/externalsecret.client-certs.yaml), the secret would only contain one key/value pair (as we only have one application) and it would be the client-id that we used previously followed by `.pem` for the key. So in our example, the key should be: `saml-application.pem` and it's value would be the certificate we downloaded earlier when setting up the AWS Identity Center application.

If you no longer have the certificate available, it can be retrieved by accessing the application in IAM Identity Center in the AWS Console, and then from the `Actions` dropdown, selecting `Edit configuration`. On the next page you can re-download the certificate.

## Standalone Dex Setup

In my [repository](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/tree/main/kustomize/dex), you can find Kustomize templates to deploy Dex

### Necessary Kustomizations

I tried to make it obvious in the repository code where modifications have to be made to make installations specific to the user. The following changes should be made in the `kustomize/dex` directory:

* `externalsecret.client-certs.yaml`
  * Update `dex/client-certs` with the name of the AWS Secret being used to hold the client certificates
* `externalsecret.saml-application-key.yaml`
  * Update `dex/saml-application-key` with the name of the AWS Secret being used for the application key
* `ing.dex.yaml`
  * `REPLACE` should be replaced with the ARN of the certificate for the domain being used for Dex
  * `dex-domain.com` should be replaced with whatever domain is being used to host Dex (it should be related to the certificate)
* `sa.dex-externalsecrets.yaml`
  * `arn:aws:iam::ACCOUNT-ID:role/ROLE-NAME`
    * `ACCOUNT-ID` should be replaced with your AWS Account ID
    * `ROLE-NAME` should be replaced with the name of your IAM role
      * This role should have access to the two secrets being used by dex (client certificates and SAML application keys)
* `values.yaml`
  * `dex-domain.com` should be updated in multiple places to match the domain Dex is being served on
  * `SAML-DOMAIN.com` should be updated to be the domain the SAML application is being hosted on
  * `ssoURL` and `ssoIssuer` both need to have the `*`s in their values replaced
    * These values were obtained when setting up the SAML Application in the IAM Identity Center
    * If you do not have these, they can be retrieved by accessing the application in IAM Identity Center in the AWS Console, and then from the `Actions` dropdown, selecting `Edit configuration`. On the next page you can obtain these values.

### PreRequisites

We rely on the External Secrets Operator for obtaining sensitive values from AWS Secrets Manager. The Custom Resource Definitions (CRDs) must be installed first. Installation information can be found [here](https://external-secrets.io/latest/introduction/getting-started/)

### Deploying

#### Kustomize / Kubectl

Usually when using Kustomize I do not use `kustomize build . | kubectl diff -f -` but prefer to use `kubectl diff -k .` as it's more straight forward, however because we are using a Helm chart, we have to use `kustomize build` here.

#### Commands

All of these commands should be ran from the `kustomize/dex` directory:

* Creating the dex namespace
  * Kustomize wants the namespace to exist prior to it being referenced, even if you are creating the namespace in the same file
  * `kubectl diff -f ns.dex.yaml`
  * If the output of the diff looks good:
    * `kubectl apply -f ns.dex.yaml`
* `kustomize build --enable-helm . | kubectl diff -f -`
  * This command will build Kubernetes manifests using Kustomize and then show you the differences from what's currently deployed
  * This will add the necessary resources for the standalone Dex deployment
* If the output of the diff looks good:
  * `kustomize build --enable-helm . | kubectl apply -f -`

## SAML Application (Argo Workflows)

A sample SAML application [is available in my repository](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/tree/main/kustomize/saml-application?ref_type=heads). We're using [Argo Workflows for this example](https://gitlab.com/archetypalsxe/dex-to-aws-access-portal-applications/-/blob/main/kustomize/saml-application/kustomization.yaml?ref_type=heads#L7) since it's what I used when I went through this and it's easily referenced with few modifications. You can either use the example as is and deploy Argo Workflows, or replace this with a SAML application of your own choosing.

### Necessary Kustomizations

Assuming that we want to deploy Argo Workflows, within the `kustomize/saml-application` directory the following changes should be made:

* `cm.workflow-controller-configmap.yaml`
  * `DEX-DOMAIN.com` - Should be set to the domain being used by external dex
  * `SAML-DOMAIN.com` - Should be set to the domain being used by the SAML application (Argo Workflows)
* `externalsecret.dex-client-key.yaml`
  * `dex/saml-application-key` - Should be the name of the secret in AWS
* `ing.ingress.yaml`
  * `saml-domain.com` - Should also be the domain being used by the SAML application
* `sa.external-secrets.yaml`
  * `eks.amazonaws.com/role-arn: "arn:aws:iam::ACCOUNT-ID:role/ROLE-NAME"`
    * `ACCOUNT-ID` should be your AWS account ID
    * `ROLE-NAME` should be the name of your AWS role being used by external secrets
  * This role should have access to the secret referenced in `dex-client-key.yaml`
  * While this role could be the same one used by the Dex deployment, it’s generally recommended to use separate roles to adhere to the principle of least privilege and minimize the blast radius in case of a compromise.

### Deploying

Since we're not using Helm, we can use `kubectl` with the `-k` flag rather than running Kustomize directly.

#### Commands

* From the `kustomize/saml-application` directory:
  * Create the namespace:
    * `kubectl diff -f ns.saml-application.yaml`
    * If the output looks good:
      * `kubectl apply -f ns.saml-application.yaml`
  * Deploy the SAML Application/Argo Workflows:
    * `kubectl diff -k .`
    * If everything looks good:
      * `kubectl apply -k .`

## Conclusion

Thank you for reading! I hope you found this article useful. Your feedback is greatly appreciated, and I welcome suggestions for improvement. If any part of the guide was unclear, or you have specific questions, feel free to reach out—I’m happy to provide clarifications. If there’s enough interest, I’m considering creating more detailed guides, video tutorials, or even a follow-up article covering OAuth 2.0 integration alongside SAML.

## References

Below is a list of references I relied on to get everything working successfully. No single reference covered all the necessary steps; it was a combination of several that ultimately provided the complete solution. This article aims to consolidate that information into one cohesive guide.

### From Argo
* [Using Argo CD's Dex Deployment for Authentication](https://argo-workflows.readthedocs.io/en/stable/argo-server-sso-argocd/)
  * This goes through how you would share the Dex installation included with Argo CD between the two Argo applications
* [Argo Server with SSO](https://argo-workflows.readthedocs.io/en/stable/argo-server-sso/)
  * Although this mainly focuses on OAuth authentication, it did provide some useful hints on the Client IDs and Secrets that are necessary even with SAML
* [ArgoCD with Identity Center / AWS SSO](https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/identity-center/#saml-with-dex)
  * Docs for Argo CD for integrating with AWS Identity Center / SSO that also contained some useful hints although not as helpful as you would assume

### From Dex
* [SAML](https://dexidp.io/docs/connectors/saml/)
  * Dex's official docs for the SAML connector
  * **Again... please note that SAML is not maintained and is a candidate to be deprecated**
* [OAUTH](https://dexidp.io/docs/connectors/oauth/)
  * Dex's official docs for the OAuth connector
  * More helpful than you would think because of it talking about the Client ID and Secret which we used quite heavily
