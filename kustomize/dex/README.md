# Standalone Dex

## Building

* `kustomize build --enable-helm .`

## Seeing a Diff

* `kustomize build --enable-helm . | kubectl diff -f -`

## Applying Changes

* `kustomize build --enable-helm . | kubectl apply -f -`