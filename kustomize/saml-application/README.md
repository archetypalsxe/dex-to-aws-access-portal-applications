# SAML Application

We are using Argo Workflows in this example

## Building

* `kustomize build .`
  * Helm isn't required with this application

## Getting a Diff
`kubectl diff -k .`

## Applying the Diff
`kubectl apply -k .`